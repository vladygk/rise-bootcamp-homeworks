﻿namespace OOP_EncapsulationInheritance.Enums;

    public enum BiomeTypes
    {
        ForestBiome,
        OceanBiome,
        DessertBiome
    }

