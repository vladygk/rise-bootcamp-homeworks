﻿namespace OOP_EncapsulationInheritance.Enums;

public enum IEatableTypes
{
    Lion,
    Shark,
    Zebra,
    Tilapia,
    Bear,
    Whale,
    Meat,
    Bone,
    Fruit,
    Vegetable,
    IceCream,
    Pizza,
    Seaweed,
    Cactus
}

