﻿namespace OOP_EncapsulationInheritance.IO;

public interface IWriter
{
    void Write(string text);
}