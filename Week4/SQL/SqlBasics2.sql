CREATE DATABASE Flights;

CREATE TABLE AIRLINE(
    CODE CHAR(2) PRIMARY KEY,
    [NAME] VARCHAR(52) NOT NULL UNIQUE,
    COUNTRY VARCHAR(50) NOT NULL
);
--AIRPLANE
CREATE TABLE AIRPLANE(
     CODE CHAR(3) PRIMARY KEY,
     [TYPE] VARCHAR(30) NOT NULL,
     SEATS INT NOT NULL,
     [YEAR] INT NOT NULL,
     CHECk (SEATS >= 0)
)


--Booking   
CREATE TABLE BOOKING(
    CODE CHAR(6) 
    PRIMARY KEY,
    AGENCY VARCHAR(50) NOT NULL,
    AIRLINE_CODE CHAR(2) NOT NULL,
    FLIGHT_NUMBER CHAR(6) NOT NULL,
    CUSTOMER_ID INT NOT NULL,
    BOOKING_DATE DATETIME2 NOT NULL,
    FLIGHT_DATE DATETIME2 NOT NULL,
    PRICE INT NOT NULL,
    [STATUS] BIT NOT NULL,
    CHECK (BOOKING_DATE<=FLIGHT_DATE)



)

CREATE TABLE FLIGHT(
    FNUMBER VARCHAR(10) PRIMARY KEY,
    AIRLINE_OPERATOR CHAR(2) NOT NULL,
    DEP_AIRPORT CHAR(3) NOT NULL,
    ARR_AIRPORT CHAR(3) NOT NULL,
    FLIGHT_TIME TIME NOT NULL,
    PRICE INT NOT NULL,
    AIRPLANE CHAR(3) NOT NULL


)

CREATE TABLE AGENCY(
    [NAME] VARCHAR(50) PRIMARY KEY,
    COUNTRY VARCHAR(50) NOT NULL,
    CITY VARCHAR(30) NOT NULL,
    PHONE VARCHAR(15) UNIQUE
)

CREATE TABLE CUSTOMER(
    ID INT PRIMARY KEY,
    FNAME VARCHAR(50) NOT NULL,
    LNAME VARCHAR(50) NOT NULL,
    EMAIL VARCHAR(30) NOT NULL UNIQUE
    CHECK(EMAIL LIKE '%@%.%' AND LEN(EMAIL)>=6)
)

CREATE TABLE AIRPORT(
    CODE CHAR(3) PRIMARY KEY,
    [NAME] VARCHAR(50) UNIQUE,
    COUNTRY VARCHAR(50) NOT NULL,
    CITY VARCHAR(50) NOT NULL
)





